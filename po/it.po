# Italian translation for libpanel.
# Copyright (C) 2022 libpanel's COPYRIGHT HOLDER
# This file is distributed under the same license as the libpanel package.
# Davide Ferracin <davide.ferracin@protonmail.com>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: libpanel main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libpanel/issues\n"
"POT-Creation-Date: 2022-09-20 18:39+0000\n"
"PO-Revision-Date: 2022-09-22 13:09+0200\n"
"Last-Translator: Davide Ferracin <davide.ferracin@protonmail.com>\n"
"Language-Team: Italian <gnome-it-list@gnome.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/panel-frame-header-bar.ui:52
msgid "Open Pages"
msgstr "Apri pagine"

#: src/panel-frame.ui:60
msgid "Frame"
msgstr "Riquadro"

#: src/panel-frame.ui:62
msgid "Move Page _Left"
msgstr "Sposta pagina a _sinistra"

#: src/panel-frame.ui:67
msgid "Move Page _Right"
msgstr "Sposta pagina a _destra"

#: src/panel-frame.ui:72
msgid "Move Page _Up"
msgstr "Sposta pagina in _alto"

#: src/panel-frame.ui:76
msgid "Move Page _Down"
msgstr "Sposta pagina in _basso"

#: src/panel-frame.ui:82
msgid "Close Frame"
msgstr "Chiudi riquadro"

#: src/panel-maximized-controls.c:73
msgid "Restore panel to previous location"
msgstr "Riporta pannello alla posizione precedente"

#: src/panel-save-dialog-row.c:78
msgid "(new)"
msgstr "(nuovo)"

#. translators: %s is replaced with the document title
#: src/panel-save-dialog.c:400
#, c-format
msgid "The draft “%s” has not been saved. It can be saved or discarded."
msgstr "La bozza «%s» non è stata salvata. Può essere salvata o scartata."

#: src/panel-save-dialog.c:403
msgid "Save or Discard Draft?"
msgstr "Salvare o scartare la bozza?"

#: src/panel-save-dialog.c:407 src/panel-save-dialog.c:427
#: src/panel-save-dialog.ui:11
msgid "_Discard"
msgstr "S_carta"

#: src/panel-save-dialog.c:411
msgid "_Save As…"
msgstr "_Salva come…"

#. translators: %s is replaced with the document title
#: src/panel-save-dialog.c:420
#, c-format
msgid "“%s” contains unsaved changes. Changes can be saved or discarded."
msgstr ""
"«%s» contiene modifiche non salvate. Le modifiche possono essere salvate "
"o scartate."

#: src/panel-save-dialog.c:423 src/panel-save-dialog.c:454
msgid "Save or Discard Changes?"
msgstr "Salvare o scartare le modifiche?"

#: src/panel-save-dialog.c:431 src/panel-save-dialog.ui:12
msgid "_Save"
msgstr "_Salva"

#: src/panel-save-dialog.c:456
msgid ""
"Open documents contain unsaved changes. Changes can be saved or discarded."
msgstr ""
"Il documento aperto contiene delle modifiche non salvate. Le modifiche possono "
"essere salvate o scartate."

#: src/panel-save-dialog.c:461
msgid "Only _Save Selected"
msgstr "_Salva solo i selezionati"

#: src/panel-save-dialog.c:469
msgid "Save All"
msgstr "Salva tutto"

#: src/panel-save-dialog.c:478
msgid "Discard All"
msgstr "Scarta tutto"

#: src/panel-save-dialog.ui:10
msgid "_Cancel"
msgstr "A_nnulla"

#: src/panel-theme-selector.ui:21 src/panel-theme-selector.ui:23
msgid "Follow system style"
msgstr "Segui lo stile del sistema"

#: src/panel-theme-selector.ui:37 src/panel-theme-selector.ui:39
msgid "Light style"
msgstr "Stile chiaro"

#: src/panel-theme-selector.ui:54 src/panel-theme-selector.ui:56
msgid "Dark style"
msgstr "Stile scuro"
